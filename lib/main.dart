import 'package:wallet_cold/pages/MainPage.dart';
import 'package:flutter/material.dart';
import 'package:wallet_cold/pages/SignFlow/SigningPage.dart';
import 'package:wallet_cold/pages/ScanPage.dart';

final ThemeData _theme = new ThemeData(
  fontFamily: 'Alef',
  primaryColor: Colors.white,
  buttonColor: Colors.blue,
  primaryIconTheme: new IconThemeData(color: Colors.blue),
);

main(){
  runApp(new MaterialApp(
      theme: _theme,
      home: new MainPage(),
      onUnknownRoute: (RouteSettings settings) {
        print(settings);
      },
      onGenerateRoute: (RouteSettings settings) {
        switch (settings.name) {
          case MainPage.routeName: return new MyCustomRoute(
            builder: (_) => new MainPage(),
            settings: settings,
          );
          case SigningPage.routeName: return new MyCustomRoute(
            builder: (_) => new SigningPage(),
            settings: settings,
          );
          case ScanPage.routeName: return new MyCustomRoute(
            builder: (_) => new ScanPage(),
            settings: settings,
          );
//          case SuccessPage.routeName: return new MyCustomRoute(
//            builder: (_) => new SuccessPage(),
//            settings: settings,
//          );
//          case WaitingConfPage.routeName: return new MyCustomRoute(
//            builder: (_) => new WaitingConfPage(),
//            settings: settings,
//          );
        }
      }
//    home: new MainPage(),
  ));
}


class MyCustomRoute<T> extends MaterialPageRoute<T> {
  MyCustomRoute({ WidgetBuilder builder, RouteSettings settings })
      : super(builder: builder, settings: settings);

  @override
  Widget buildTransitions(BuildContext context,
      Animation<double> animation,
      Animation<double> secondaryAnimation,
      Widget child) {

    if (settings.isInitialRoute)
      return child;
    // Fades between routes. (If you don't want any animation,
    // just return child.)
    return new FadeTransition(opacity: animation, child: child);
  }
}