import 'dart:async';

enum AppLocale {EN}

class MainConfig{
  static bool isProduction = true;

  static AppLocale locale = AppLocale.EN;

  static String endpointURL = '';

  static String version = '1.0.0';

  static String TXIDnotsigned = 'askjdhaskdjashckashcksajchaskjchkasjhckajsckasjcaskjcaksjbss';

  static String TXIDsigned;

  static Future<String> scannedData;
}