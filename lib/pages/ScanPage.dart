import 'package:wallet_cold/config/main.dart';
import 'package:flutter/material.dart';
import 'package:qr_mobile_vision/qr_camera.dart';
import 'package:wallet_cold/controllers/HelperCtrl.dart';
import 'package:wallet_cold/controllers/StyleCtrl.dart';
import 'dart:async';

import 'package:wallet_cold/pages/SignFlow/SigningPage.dart';

class ScanPage extends StatefulWidget {
  static const String routeName = '/Scan';

  @override
  State<StatefulWidget> createState() => new ScanPageState ();
}

class ScanPageState extends State<ScanPage> {
  StyleCtrl style = new StyleCtrl();
  bool isActive = true;

  @override
  Widget build(BuildContext context) {
    style.getSizes(context);

    return new Scaffold(
      backgroundColor: new Color.fromRGBO(255, 255, 255, 0.5),

      appBar: new AppBar(
        title: new Text('Scan QR Code'),
      ),
      body: new Center(
        child: new Container(
          width: style.width,
          color: Colors.white,
            foregroundDecoration: isActive ? new BoxDecoration(
              image: new DecorationImage(
                image: new AssetImage("images/scanFrame.png",),
                fit: BoxFit.cover,
                colorFilter: new ColorFilter.mode(Colors.transparent, BlendMode.screen),
              ),
            ) : null,
            child: isActive ? new QrCamera(
              fit: BoxFit.cover,
              qrCodeCallback: (code) {
                setState(() {
                  isActive = false;
                });

                new Future.delayed(new Duration(seconds: 10), (){
                  MainConfig.scannedData = new Future<String>(()=>code);
                  Navigator.popAndPushNamed(context, SigningPage.routeName);
                });

              },
            ): new Column(
              mainAxisAlignment: MainAxisAlignment.start,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: <Widget>[
                new Padding(padding: new EdgeInsets.symmetric(vertical: 80.0),child: new Text(HelperCtrl.t("Signing Transaction..."), style: style.getTitleStyle(color: Colors.grey),),),

                new CircularProgressIndicator()
              ],
            )
        ),
      ),
    );
  }
  
}