import 'dart:async';

import 'package:qr_reader/qr_reader.dart';
import 'package:wallet_cold/config/main.dart';
import 'package:wallet_cold/controllers/HelperCtrl.dart';
import 'package:wallet_cold/controllers/ServerCtrl.dart';
import 'package:wallet_cold/controllers/StyleCtrl.dart';
import 'package:wallet_cold/pages/OKWidget.dart';
import 'package:wallet_cold/pages/ScanPage.dart';
import 'package:wallet_cold/pages/SignFlow/SigningPage.dart';
import 'package:wallet_cold/widgets/ActionButtonWidget.dart';
import 'package:wallet_cold/widgets/GenerateWidget.dart';
import 'package:wallet_cold/widgets/SpinnerWidget.dart';
import 'package:flutter/material.dart';
//import 'package:charts_flutter/flutter.dart';
import 'package:wallet_cold/widgets/TransactionsWidget.dart';

class MainPage extends StatefulWidget {
  static const String routeName = '/Main';

  @override
  State<StatefulWidget> createState() => new MainPageState ();
}

class MainPageState extends State<MainPage> {

  StyleCtrl style = new StyleCtrl();
  GlobalKey<SpinnerWidgetState> _spinnerKey = new GlobalKey<SpinnerWidgetState>();
  Server server;

  Color color = Colors.black;
  String errorMsg = '';

  MainPageState(){
    server = new Server(spinnerKey: _spinnerKey, setState: setState);
  }

  String msg = HelperCtrl.t("Loading...");

  @override
  void initState() {
    super.initState();
  }


  @override
  Widget build(BuildContext context) {
    GenerateWidget.startContext = context;
    style.getSizes(context);

    return new SpinnerWidget(new Scaffold(
      appBar: GenerateWidget.getAppBar(new Center(child: new Text(""),), style, context),
        body:
          new Center(
            child: new Container(
                child: new Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: <Widget>[

//                    new OKWidget(child: new Text("ok", style: new TextStyle(fontSize: 50.0),),),

                    new Padding(padding: new EdgeInsets.symmetric(horizontal: style.getOptimalSize(40.0),vertical: style.getOptimalSize(100.0)), child: new Image.asset("images/logo.jpg"),),

                    new Padding(padding: new EdgeInsets.only(bottom: 30.0), child: new Text(HelperCtrl.t("Tap to Sign"), style: style.getTitleStyle(color: Colors.grey), textAlign: TextAlign.center,),),

                    new FlatButton(
                      onPressed: (){
                        Navigator.pushNamed(context, ScanPage.routeName);
                      },
                      child: new Image.asset("images/scan_button.png", width: style.width/2,)
                    )

                  ],
                )
            ),
          )),
      msg, key: _spinnerKey,);
  }
}

