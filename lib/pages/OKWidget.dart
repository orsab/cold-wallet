import 'package:wallet_cold/config/main.dart';
import 'package:flutter/material.dart';
import 'package:qr_mobile_vision/qr_camera.dart';
import 'package:wallet_cold/controllers/StyleCtrl.dart';
import 'dart:async';

import 'package:wallet_cold/pages/SignFlow/SigningPage.dart';

class OKWidget extends StatefulWidget {
  const OKWidget({Key key, this.child}) : super(key: key);

  @override
  State<StatefulWidget> createState() => new OKWidgetState ();

  final Widget child;
}

class OKWidgetState extends State<OKWidget> with SingleTickerProviderStateMixin {
  AnimationController controller;
  Animation<double> animation;
  Future<bool> future;

  @override
  Widget build(BuildContext context) {

    return new FutureBuilder(future: future, builder: (BuildContext context, AsyncSnapshot<bool>snapshot){
      if(snapshot.data != null){
        return widget.child;
      }
      return new Container(
        width: 200.0,
        height: 200.0,
        child: new Container(
          width: animation.value * 150.0,
          decoration: new BoxDecoration(
            shape: BoxShape.circle,
            color: new Color.fromRGBO(0, 118, 255, 0.1),
          ),
          child: new Padding(padding: new EdgeInsets.all(30.0), child: new Container(
            child: new Icon(
              Icons.check,
              size: animation.value * 130.0,
              color: new Color.fromRGBO(0, 118, 255, 1.0),
            ),
          ),),
        ),
      );
    });
  }

  @override
  void initState() {
    super.initState();

    controller = new AnimationController(duration: new Duration(milliseconds: 1000),vsync: this);
    animation = new CurvedAnimation(parent: controller, curve: Curves.bounceIn);

    animation.addListener((){
      setState(() {});
    });

    animation.addStatusListener((AnimationStatus status){

    });

    controller.forward();

    future = new Future<bool>.delayed(new Duration(seconds: 3), (){
      return true;
    });
  }

  @override
  void dispose() {
    controller.dispose();
    super.dispose();
  }


}