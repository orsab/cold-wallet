import 'dart:async';

import 'package:flutter/material.dart';
import 'package:wallet_cold/config/main.dart';
import 'package:wallet_cold/controllers/HelperCtrl.dart';
import 'package:wallet_cold/controllers/StyleCtrl.dart';
import 'package:wallet_cold/pages/OKWidget.dart';
import 'package:wallet_cold/pages/ScanPage.dart';
import 'package:wallet_cold/widgets/ActionButtonWidget.dart';
import 'package:wallet_cold/widgets/GenerateWidget.dart';
import 'package:qr_flutter/qr_flutter.dart';
import 'package:qr_reader/qr_reader.dart';

class SigningPage extends StatefulWidget {
  static const String routeName = '/SigningPage';

  @override
  State<StatefulWidget> createState() => new SigningPageState ();
}

class SigningPageState extends State<SigningPage> {
  StyleCtrl style = new StyleCtrl();
  Color color = Colors.black;

  String errorMsg = '';
  Widget title = new Container();
  Future<String> signedTXFuture;


  @override
  void initState() {
    super.initState();
//    _init();
  }

  @override
  Widget build(BuildContext context) {
    style.getSizes(context);

    return new Scaffold(
      appBar: GenerateWidget.getAppBar(new Center(child: new Text(""),), style, context),
      body: new Center(
        child: new Container(
          padding: new EdgeInsets.symmetric(horizontal: style.getOptimalSize(50.0)),
          child: new Column(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.center,
            children: <Widget>[

              new Padding(padding: new EdgeInsets.symmetric(vertical: 80.0), child: new Text(HelperCtrl.t("Transaction Signed"), style: style.getTitleStyle(color: Colors.grey),),),

              new OKWidget(child: new FutureBuilder(builder: (BuildContext context, AsyncSnapshot snapshot){
                if(snapshot.data != null){

                  return new Padding(
                      padding: new EdgeInsets.all(10.0),
                      child: new Column(
                        children: <Widget>[
                          new QrImage(
                            foregroundColor: Colors.blue,
//                          data: snapshot.data,
                            version: 7,
                            data: snapshot.data,
                            size: 200.0,
                          ),

                          new Padding(
                            padding: new EdgeInsets.all(20.0),
                            child: new ActionButtonWidget((){
                              Navigator.pop(context);
                            },
                              "OK", style,
                            ),
                          ),
                        ],
                      )
                  );
                }
                return new Padding(padding: new EdgeInsets.all(100.0), child: new Container(),);
              },
                future: MainConfig.scannedData,
              ),),

              new Padding(padding: new EdgeInsets.all(10.0), child: new Text(errorMsg, style: style.getTitleStyle(color: Colors.red),),),

            ],
          ),
        ),
      ),
    );
  }

  
}