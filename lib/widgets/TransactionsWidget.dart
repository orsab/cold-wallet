import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:wallet_cold/controllers/StyleCtrl.dart';

class TransactionsWidget extends StatefulWidget {

  final StyleCtrl style;

  const TransactionsWidget({Key key, this.style}) : super(key: key);

  @override
  State<StatefulWidget> createState() => new TransactionsWidgetState();

}

class TransactionsWidgetState extends State<TransactionsWidget> with SingleTickerProviderStateMixin {
  TabController controller;

  int lastIndex;

  List<TXData> alldata = <TXData>[
    new TXData('kjwdcweegwefiwgef', 0.122323, new DateTime(2018), true),
    new TXData('kjsdvsdsgwiwgef', 1.122323, new DateTime(2011), false),
    new TXData('sdsdgsgdgwgef', 2.122323, new DateTime(2012), true),
    new TXData('23423dfsdfwefiwgef', 0.234344, new DateTime(2013), false),
    new TXData('avvdsvsdvfiwgef', 0.2324324234, new DateTime(2014), true),
    new TXData('rgergreggwefiwgef', 0.234234234, new DateTime(2015), true),
  ];
  
  List<TXData> data;

  @override
  Widget build(BuildContext context) {
    if(controller == null) {
      data = alldata;

      controller = new TabController(length: 3, vsync: this,);
      controller.addListener((){
        if(lastIndex != controller.index){
          switch(controller.index){
            case 0:
              setState(() {
                data = alldata;
              });
              break;
            case 1:
              setState(() {
                data = alldata.where((TXData item) => item.type).toList();
              });
              break;
            case 2:
              setState(() {
              data = alldata.where((TXData item) => !item.type).toList();
              });
              break;
          }
          

          lastIndex = controller.index;
        }

      });
    }

    return new Scaffold(
      appBar: new AppBar(
        elevation: 0.0,
        backgroundColor: Colors.white24,
        title: new Material (
          child: new TabBar(
            indicatorColor: Colors.blue,
            tabs: [
              new Tab(text: "All",),
              new Tab(text: "Recived",),
              new Tab(text: "Received",),
            ],
            controller: controller,
          ),
        ),
      ),
      body: new ListView(
        children: data.map((TXData row) => new Card(
          child: new Padding(padding: new EdgeInsets.all(5.0), child: new Row(
            mainAxisAlignment: MainAxisAlignment.spaceBetween,
            children: <Widget>[
              new Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: <Widget>[
                  new Text(row.id, style: widget.style.getTitleStyle( isBold: true),),
                  new Text(new DateFormat("dd.MM.yyyy").format(row.datetime), style: widget.style.getTitleStyle(color: Colors.grey),),
                ],
              ),


              new Row(
                children: <Widget>[
                  new Padding(padding: new EdgeInsets.symmetric(horizontal: 20.0), child: new Text(row.type ? "+" : "-", textAlign: TextAlign.right, style: widget.style.getTitleStyle(size:60.0, isBold:true, color: row.type ? Colors.greenAccent : Colors.redAccent),),),
                  new Text(row.amount.toStringAsFixed(8), textAlign: TextAlign.right, style: widget.style.getTitleStyle(isBold: true),)
                ],
              )
            ],
          ),),
        )).toList(),
      ),
    );
  }
}

class TXData{
  final String id;
  final double amount;
  final DateTime datetime;
  final bool type;

  TXData(this.id, this.amount, this.datetime, this.type);
}