import 'package:flutter/widgets.dart';
import 'package:flutter/material.dart';

class SpinnerWidget extends StatefulWidget {
  final String message;

  final Widget child;

  final Color messageColor;

  final Color smokeScreenColor;

  SpinnerWidget(this.child, this.message,
      {this.messageColor: const Color(0xFFFFFFFF), Key key,
        this.smokeScreenColor: const Color.fromRGBO(0, 0, 0, 0.7)}):super(key:key);

  @override
  SpinnerWidgetState createState() => new SpinnerWidgetState();
}

class SpinnerWidgetState extends State<SpinnerWidget> {
  bool active = false;
  
  @override
  Widget build(BuildContext context) {
    if (active) {
      final stacks = <Widget>[new Positioned.fill(child: widget.child)];

      /// If there is a message, show the message
      if (widget.message is String) {
        stacks.add(new Positioned.fill(
          child: new Container(
//              color: widget.smokeScreenColor,
              child: new Center(
                  child: new Column(
                    mainAxisSize: MainAxisSize.min,
                    children: <Widget>[
                      new CircularProgressIndicator(),
                      new Container(margin: new EdgeInsets.only(top: 20.0)),
                      new Text(widget.message,
                          style: new TextStyle(
                              color: widget.messageColor, fontSize: 16.0)),
                    ],
                  ))),
        ));
      }

      return new Stack(children: stacks);
    }
    
    return widget.child;
  }
}