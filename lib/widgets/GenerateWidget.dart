import 'dart:async';
import 'dart:convert';

import 'package:wallet_cold/config/main.dart';
import 'package:wallet_cold/controllers/HelperCtrl.dart';
import 'package:wallet_cold/controllers/StyleCtrl.dart';
import 'package:wallet_cold/pages/MainPage.dart';
import 'package:flutter/material.dart';

class GenerateWidget {
  static BuildContext startContext;

  static Future<int> showCustomDialog(BuildContext context, callback,
      StyleCtrl style,
      {isAlert = true, title = 'System message', content = '', btnText}) {
    Completer _completer = new Completer();

    var alert = new AlertDialog(
      title: new Text(HelperCtrl.t(title), textAlign: TextAlign.center,),
      content: new SingleChildScrollView(
        child: new ListBody(
          children: <Widget>[
            new Text(HelperCtrl.t(content),
              textAlign: style.isRtl ? TextAlign.right : TextAlign.left,
              textDirection: style.getTextDirection(),
              style: new TextStyle(fontWeight: FontWeight.bold),),
          ],
        ),
      ),
      actions: <Widget>[
        new FlatButton(
          onPressed: () {
            Navigator.pop(context);
            _completer.complete(0);
          },
          child: new Text(HelperCtrl.t(btnText != null ? btnText : "Ok")),
        ),
        !isAlert ? new IconButton(
          icon: new Icon(Icons.refresh),
          onPressed: () {
            _completer.complete(0);
            callback();
          },
          color: Colors.blue,
        ) : new Container()
      ],
    );

    showDialog(
        barrierDismissible: false,
        context: context,
        child: alert
    );

    return _completer.future;
  }

  static void popUntilRoot(BuildContext context) {
    if (GenerateWidget.startContext == null)
      Navigator.pop(context);
    else {
      bool pop = true;
      try {
        Navigator.popUntil(context, (dynamic route) {
          print(route);

          if (pop) {
            if (route.toString().startsWith('MaterialPageRoute<dynamic>("/"')) {
              pop = false;
              return true;
            }
            return false;
          }
          else {
            return true;
          }
        });
      } catch (ex) {
        pop = false;
      }
    }
  }


  static getAppBar(Widget title, StyleCtrl style, BuildContext context) {
    return new AppBar(
//      primary: true,
      elevation: 0.0,
      title: title,
      backgroundColor: Colors.white24,
//      actions: <Widget>[
//
//        new PopupMenuButton(
//          onSelected: (Choice item) {
//            switch (item.name) {
//              case 'balance':
//                break;
//            }
//          }, icon: new Icon(Icons.dehaze,),
//          itemBuilder: (BuildContext context) {
//            return <Choice>[
//              new Choice(name: "balance",
//                  title: HelperCtrl.t("Balance"),
//                  icon: Icons.account_balance_wallet),
//            ].map((Choice choice) {
//              return new PopupMenuItem<Choice>(
//                  value: choice,
//                  child: new Container(
//                    width: style.getOptimalSize(style.width),
//                    child: new Row(children: <Widget>[
//                      new IconButton(
//                          icon: new Icon(choice.icon), onPressed: null),
//                      new Text(choice.title)
//                    ],),
//                  )
//              );
//            }).toList();
//          },
//        ),
//
//
//      ],
    );
  }
}

class Choice {

  const Choice({this.name, this.title, this.icon});

  final String name;
  final String title;
  final IconData icon;
}